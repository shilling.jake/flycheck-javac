;;; flycheck-javac.el --- On the Fly Checker using JavaC-*- lexical-binding: t; -*-
;;
;; Copyright (C) 2021 Jake Shilling
;;
;; Author: Jake Shilling <https://github.com/j-shilling>
;; Maintainer: Jake Shilling <shilling.jake@gmail.com>
;; Created: September 27, 2021
;; Modified: September 27, 2021
;; Version: 0.0.1
;; Homepage: https://github.com/j-shilling/flycheck-javac
;; Package-Requires: ((emacs "25.1") (flycheck "0.22") (dash "2.19.1"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  Description
;;
;;; Code:

(require 'dom)
(require 'flycheck)
(require 'files)
(require  'subr-x)

(defun flycheck-javac--maven-executable
    ()
  (executable-find "mvn"))

(defun flycheck-javac--maven-pom-file
    ()
  (when-let ((pom-dir (locate-dominating-file "." "pom.xml")))
    (expand-file-name "pom.xml" pom-dir )))

(defun flycheck-javac--maven-properties ()
  (when-let ((mvn (flycheck-javac--maven-executable))
             (pom (flycheck-javac--maven-pom-file)))
    (with-temp-buffer
      (let ((status-code (call-process mvn nil t nil
                                       "-f" pom "help:effective-pom")))
        (when (eq status-code 0)
          (let ((end (+ (search-backward "</project>") (length "</project>")))
                (beginning (search-backward "<project ")))
            (libxml-parse-xml-region beginning end)))))))

(defun flycheck-javac--source-path ()
  (when-let ((props (flycheck-javac--maven-properties)))
    (->> (dom-by-tag props 'sourceDirectory)
         dom-text)))

(defun flycheck-javac--source ()
  (when-let ((props (flycheck-javac--maven-properties)))
    (->> (dom-by-tag props 'maven.compiler.source)
         dom-text)))

(defun flycheck-javac--release ()
  (when-let ((props (flycheck-javac--maven-properties)))
    (->> (dom-by-tag props 'maven.compiler.target)
         dom-text)))

(defun flycheck-javac--maven-classpath
    ()
  (when-let ((mvn (flycheck-javac--maven-executable))
             (pom (flycheck-javac--maven-pom-file)))
    (with-temp-buffer
      (let ((status-code (call-process mvn nil t nil
                                       "-f" pom "dependency:build-classpath")))
        (when (eq status-code 0)
          (goto-char (search-backward "[INFO] Dependencies classpath:"))
          (beginning-of-line 2)
          (string-trim (thing-at-point 'line)))))))

(defun flycheck-javac--generic-sentinel-builder (buffer stderr parse-output callback)
  "Return a new sentinel that passes process output to CALLBACK.

Constructs a closure and returns a function that can act as a
sentinel for the asynchronous process constructed by
`make-process'. BUFFER is expected to be the process' buffer and
STDERROR its stderr pipe.

When this sentinel is called if will check that the process has
completed successfully. If it has, then BUFFER will be passed to
PARSE-OUTPUT and the result then passed to CALLBACK. BUFFER and
STDERR will then be killed.

If the process finished with a failed exit status, then an error
will be thrown and neither BUFFER nor STDERROR will be killed."

  (lambda (proc _status)
    (unless (process-live-p proc)
     (if (equal 0 (process-exit-status proc))
         (let ((output (funcall parse-output buffer)))
           (kill-buffer buffer)
           (kill-buffer stderr)
           (funcall callback output))
       (error
        "Could not get class path from maven")))))

;;
;; Get Classpath From Maven
;;

(defun flycheck-javac--classpath-from-maven-output (buffer)
  "Return the classpath string from maven's build-classpath output.

Assume BUFFER contains the output from a successful call to
maven's build-classpath goal and return the line containing the
classpath string."

    (with-current-buffer buffer
      (let ((point (search-backward "Dependencies classpath:")))
           (goto-char point)
           (beginning-of-line 2)
           (-> (thing-at-point 'line)
               string-trim))))

(defun flycheck-javac--maven-classpath-sentinel-builder (buffer stderr callback)
  "Return a new sentinel to pass classpath from maven to CALLBACK.

Constructs a closure and returns a function that can act as a
sentinel for the asynchronous process constructed by
`flycheck-javac--maven-classpath-async'. BUFFER is expected to be
the process' buffer and STDERROR its stderr pipe.

When this sentinel is called if will check that the process has
completed successfully. If it has, then the classpath string will
be read from BUFFER and passed as the only argument to CALLBACK.
BUFFER and STDERR will then be killed.

If the process finished with a failed exit status, then an error
will be thrown and neither BUFFER nor STDERROR will be killed."

  (flycheck-javac--generic-sentinel-builder
     buffer stderr
     #'flycheck-javac--classpath-from-maven-output
     callback))

(defun flycheck-javac--maven-classpath-async (callback)
  "Build classpath with maven asynchronously.

Start an asynchronous process with maven to build the class path
for the current project. Upon successful completion call CALLBACK
passing the class path string as the only argument."

  (when-let ((mvn (flycheck-javac--maven-executable))
             (pom (flycheck-javac--maven-pom-file)))
    (let* ((buffer (generate-new-buffer "*mvn-classpath*"))
           (stderr (generate-new-buffer "*mvn-classpath:stderr*"))
           (sentinel (flycheck-javac--maven-classpath-sentinel-builder buffer
                                                                       stderr
                                                                       callback)))
      (make-process :name "mvn-build-classpath"
                    :buffer buffer
                    :stderr stderr
                    :sentinel sentinel
                    :command `(,mvn "-f" ,pom "dependency:build-classpath")))))

;;
;; Get Props from Effective Pom
;;

(defun flycheck-javac--effective-pom-from-maven-output (buffer)
  "Return the effective poms from maven's output.

Assume BUFFER contains the output from a successful call to
maven's build-classpath goal and return the effective pom parsed
with `libxml-parse-xml-region'."

    (with-current-buffer buffer
      (let ((end (+ (search-backward "</project>") (length "</project>")))
                (beginning (search-backward "<project ")))
            (libxml-parse-xml-region beginning end))))

(defun flycheck-javac--maven-effective-pom-sentinel-builder (buffer stderr callback)
  "Return a new sentinel to pass effective pom from maven to CALLBACK.

Constructs a closure and returns a function that can act as a
sentinel for the asynchronous process constructed by
`flycheck-javac--maven-effective-pom-async'. BUFFER is expected to be
the process' buffer and STDERROR its stderr pipe.

When this sentinel is called if will check that the process has
completed successfully. If it has, then the effective pom string
will be read from BUFFER, parsed with `libxml-parse-xml-region',
and passed as the only argument to CALLBACK. BUFFER and STDERR
will then be killed.

If the process finished with a failed exit status, then an error
will be thrown and neither BUFFER nor STDERROR will be killed."

  (flycheck-javac--generic-sentinel-builder
     buffer stderr
     #'flycheck-javac--effective-pom-from-maven-output
     callback))

(defun flycheck-javac--maven-effective-pom-async (callback)
  (when-let ((mvn (flycheck-javac--maven-executable))
             (pom (flycheck-javac--maven-pom-file)))
    (let* ((buffer (generate-new-buffer "*mvn-effective-pom*"))
           (stderr (generate-new-buffer "*mvn-effective-pom:stderr*"))
           (sentinel (flycheck-javac--maven-effective-pom-sentinel-builder buffer
                                                                           stderr
                                                                           callback)))
      (make-process :name "mvn-effective-pom"
                    :buffer buffer
                    :stderr stderr
                    :sentinel sentinel
                    :command `(,mvn "-f" ,pom "help:effective-pom")))))

(defun flycheck-javac--effective-pom-to-props (dom)
  (let* ((children (dom-children dom))
         (props (->>  children
                      (--find (eq 'properties (car it)))
                      dom-children
                      (--map (cons (dom-tag it) (dom-text it))))))
    (->> children
         (--find (eq 'build (car it)))
         dom-children
         (--reduce-from (if (-contains-p '(sourceDirectory testSourceDirectory) (dom-tag it))
                            (cons (cons (dom-tag it) (dom-text it)) acc)
                          acc)
                        props))))

(defun flycheck-javac--read-props-from-maven (callback)
  (flycheck-javac--maven-effective-pom-async
   (lambda (dom)
     (flycheck-javac--maven-classpath-async
      (lambda (classpath)
        (let ((props (flycheck-javac--effective-pom-to-props dom)))
          (->> (cons (cons 'classpath classpath) props)
               (funcall callback))))))))

(flycheck-define-checker javac
  "Checker for javac"
  :command ("javac" "-Xlint"
            "-sourcepath" (eval (flycheck-javac--source-path))
            "-classpath" (eval (flycheck-javac--maven-classpath))
            "-source" (eval (flycheck-javac--source))
            "-target" (eval (flycheck-javac--release))
            source)
  :error-patterns
    ((error line-start (file-name) ":" line ": error: " (message) line-end))
  :modes java-mode)

(provide 'flycheck-javac)
;;; flycheck-javac.el ends here
